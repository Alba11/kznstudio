package servlets;

import service.ProjectService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/confirmedProject")
public class ConfirmedProjectServlet extends HttpServlet {
    private final ProjectService projectService = new ProjectService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int project_id = Integer.parseInt(req.getParameter("project_id"));

        projectService.completeProject(project_id, new Date());

        resp.sendRedirect("/profile");
    }
}
