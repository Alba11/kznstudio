package servlets;

import service.CompletedProjectService;
import service.ReviewService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/review")
public class ReviewServlet extends HttpServlet {
    private final ReviewService reviewService = new ReviewService();
    private final CompletedProjectService completedProjectService = new CompletedProjectService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("completed_project_id"));
        req.setAttribute("reviews", reviewService.getReviewsByCompletedProjectId(id));
        req.setAttribute("project", completedProjectService.getCompletedProjectById(id));
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/WEB-INF/view/review.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("completed_project_id"));
        req.setAttribute("reviews", reviewService.getReviewsByCompletedProjectId(id));
        req.setAttribute("project", completedProjectService.getCompletedProjectById(id));
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/WEB-INF/view/review.jsp");
        dispatcher.forward(req, resp);
    }
}
