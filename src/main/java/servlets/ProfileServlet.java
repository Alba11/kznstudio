package servlets;

import dto.CompletedProject;
import service.CompletedProjectService;
import service.ProjectService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {
    private final ProjectService projectService = new ProjectService();
    private final CompletedProjectService completedProjectService = new CompletedProjectService();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.setAttribute("works", projectService.getProjectsByUserId((Integer) req.getSession().getAttribute("user_id")));
        req.setAttribute("completedWorks", completedProjectService.getCompletedProjectsByUserId((Integer) req.getSession().getAttribute("user_id")));
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/profile.jsp");
        dispatcher.forward(req, resp);
    }
}
