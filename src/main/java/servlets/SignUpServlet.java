package servlets;

import dto.User;
//import javaBack.UserDataBase;
import service.SessionService;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/signUp")
public class SignUpServlet extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/signUp.jsp");
        dispatcher.forward(req, resp);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("pass");
//        UserDataBase.addNewUser(username, password);
        System.out.println(username);
        UserService userService = new UserService();
        SessionService sessionService = new SessionService();
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        userService.enrollUser(username, password);

        req.getSession().setAttribute("user", username);
        req.getSession().setAttribute("username", username);
        req.getSession().setAttribute("user_id", user.getUser_id());
        if (req.getParameter("checkbox") != null) {
//            rememberSession(req, resp, username);
            sessionService.enrollSession(req.getSession().getId(), userService.findUser(username, password).getUser_id());
            Cookie cookie = new Cookie("session_id", req.getSession().getId());
            cookie.setDomain(req.getServerName());
            cookie.setPath(req.getContextPath());
            cookie.setMaxAge(60 * 60 * 24 * 30);
            resp.addCookie(cookie);
        }
        resp.sendRedirect(req.getContextPath() + "/index");
    }

//    private static void rememberSession(HttpServletRequest req, HttpServletResponse resp, String username) {
//        String sessionID = UserDataBase.generateSessionID(username);
//        Cookie cookie = new Cookie("session_id", sessionID);
//        cookie.setDomain(req.getServerName());
//        cookie.setPath(req.getContextPath());
//        cookie.setMaxAge(60 * 60 * 24 * 30);
//        resp.addCookie(cookie);
//    }
}
