package servlets;


import service.CompletedProjectService;
import service.ProjectService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/works")
public class WorksServlet extends HttpServlet {
    private final CompletedProjectService completedProjectService = new CompletedProjectService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("works", completedProjectService.getAllCompletedProjects());
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/WEB-INF/view/works.jsp");
        dispatcher.forward(req, resp);
    }
}
