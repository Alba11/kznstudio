package servlets;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import service.ProjectService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;

@WebServlet("/createProject")
@MultipartConfig
public class CreateProjectServlet extends HttpServlet {
    private final ProjectService projectService = new ProjectService();
    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String projectName = req.getParameter("project_name");
        int userId = (Integer) req.getSession().getAttribute("user_id");
        System.out.println(userId);
        Part file = req.getPart("file");

        String url = req.getParameter("project_url");

        String imagePath = "/Users/rishatlatypov/Desktop/kznstudio/uploudImages/" + projectName + "-" + file.getSubmittedFileName();

        IOUtils.copyLarge(file.getInputStream(), new FileOutputStream(imagePath));

        String bdPath = projectName + "-" + file.getSubmittedFileName();

        projectService.saveProject(bdPath, bdPath, url, userId, new Date());

        resp.sendRedirect("/profile");
    }
}
