package servlets;

//import javaBack.UserDataBase;

import service.SessionService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/signOut")
public class LogOutServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        HttpSession session = req.getSession();
//        String username = (String) session.getAttribute("user");
//        UserDataBase.deleteSessionForUser(username);
        session.removeAttribute("username");
        session.removeAttribute("password");
        session.removeAttribute("user");
        SessionService sessionService = new SessionService();
        sessionService.removeSessionByUserId((int)session.getAttribute("user_id"));
        session.removeAttribute("user_id");

        Cookie cookie = new Cookie("session_id", "expired");
        cookie.setMaxAge(0);
        resp.addCookie(cookie);
        req.getSession().invalidate();
        resp.sendRedirect("/index");
    }
}
