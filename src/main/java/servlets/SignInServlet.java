package servlets;

import dto.Session;
import dto.User;
//import javaBack.UserDataBase;
import service.SessionService;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("pass");
        String checkbox = req.getParameter("checkbox");

        UserService userService = new UserService();
        SessionService sessionService = new SessionService();
        User user = userService.findUser(username, password);

        resp.setContentType("text/html");
        PrintWriter printWriter = resp.getWriter();
//        if (UserDataBase.isUserDataCorrect(login, password)) {
//            req.getSession().setAttribute("user", login);
//            if (checkbox != null) {
//                rememberSession(req, resp, login);
//            }
//            resp.sendRedirect(req.getContextPath() + "/");
//        } else {
//            showAlert("Incorrect username or password.", printWriter);
//        }

        if (user == null) {
            showAlert("Incorrect username or password.", printWriter);
        }else{
            req.getSession().setAttribute("user", user);
            req.getSession().setAttribute("username", username);
            req.getSession().setAttribute("user_id", user.getUser_id());
            if (checkbox != null) {
                sessionService.enrollSession(req.getSession().getId(), user.getUser_id());
                Cookie cookie = new Cookie("session_id", req.getSession().getId());
                cookie.setDomain(req.getServerName());
                cookie.setPath(req.getContextPath());
                cookie.setMaxAge(60 * 60 * 24 * 30);
                resp.addCookie(cookie);
            }
            resp.sendRedirect(req.getContextPath() + "/index");
        }
        printWriter.close();
    }

    private static void showAlert(String message, PrintWriter pw) {
        pw.println("<script type=\"text/javascript\">");
        pw.println("alert('" + message + "');");
        pw.println("location='/signIn';");
        pw.println("</script>");
    }

//    private static void rememberSession(HttpServletRequest req, HttpServletResponse resp, String username) {
//        String sessionID = UserDataBase.generateSessionID(username);
//        Cookie cookie = new Cookie("session_id", sessionID);
//        cookie.setDomain(req.getServerName());
//        cookie.setPath(req.getContextPath());
//        cookie.setMaxAge(60 * 60 * 24 * 30);
//        resp.addCookie(cookie);
//    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/WEB-INF/view/signIn.jsp");
        dispatcher.forward(req, resp);
    }
}
