package servlets.validate;

import dto.User;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/signInValidate")
public class SignInValidate extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        resp.setCharacterEncoding("UTF-8");

        UserService userService = new UserService();

        if (username.equals("")) {
            resp.getWriter().write("emptyUsername ");
        }else if (password.equals("")){
                resp.getWriter().write("emptyPassword ");
        }else if (username.equals("")) {
            resp.getWriter().write("emptyUsername ");
        }else if (password.equals("")){
            resp.getWriter().write("emptyPassword ");
        }else {
            User user = userService.findUser(username, password);
            if (user == null) {
                resp.getWriter().write("userNotFound ");
            }
        }

    }
}
