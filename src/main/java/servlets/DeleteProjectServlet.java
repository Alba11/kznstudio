package servlets;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import service.ProjectService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

@WebServlet("/deleteProject")
public class DeleteProjectServlet extends HttpServlet {
    private final ProjectService projectService = new ProjectService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int projectId = Integer.parseInt(req.getParameter("project_id"));
        String image = req.getParameter("project_image");
        projectService.deleteProjectById(projectId);
        System.out.println(projectId);
        resp.sendRedirect("/profile");
    }
}
