package dto;

import lombok.Data;

import java.sql.Date;

@Data
public class CompletedProject {
    private int id;
    private int project_id;
    private int user_id;
    private String name;
    private String image_url;
    private String site_url;
    private Date create_date;
    private Date finish_date;
}
