package service;

import dao.UserDao;
import dao.UserDaoMySql;
import dto.User;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class UserService {
    private static final String SALT = "qaz123qwerty456bruh";
    private UserDao userDao = new UserDaoMySql();

    public User findUser(String username, String password){
        if(username != null && password != null){
            User userData = userDao.findUser(username);
            if (userData == null) return null;
            String hashPassword = generateSecurePassword(password);
            if(hashPassword.equals(userData.getPassword())){
                return userData;
            }
            return null;
        }
        return null;
    }

    public User getUserById(int id){
        if(id >= 0){
            User userData = userDao.getUserById(id);
            if (userData == null) return null;
            return userData;
        }
        return null;
    }

    public List<String> enrollUser(String username, String password) {
        List<String> errors = new ArrayList<>();

        if (username == null) {
            errors.add("`Username is empty. Enter the name.");
        }
        if (password == null) {
            errors.add("Password is empty. Enter the password.");
        }
        if (!errors.isEmpty()) {
            return errors;
        }

        User user = new User();
        user.setUsername(username);
        String pass = generateSecurePassword(password);
        user.setPassword(pass);


        userDao.saveUser(user);
        System.out.println("userService is done");
        return null;
    }

    private String generateSecurePassword(String password) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-512");
            messageDigest.update(SALT.getBytes());
            byte[] newValue = messageDigest.digest(password.getBytes(StandardCharsets.UTF_8));
            return java.util.Base64.getEncoder().encodeToString(newValue);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return password;
    }

    public boolean userIsExist(String username){
        User user = userDao.findUser(username);
        return user != null;
    }
}
