package service;

import dao.ReviewDao;
import dao.ReviewDaoMySql;
import dto.Review;

import java.util.Date;
import java.util.List;

public class ReviewService {
    private final ReviewDao reviewDao = new ReviewDaoMySql();

    public boolean saveReview(int user_id, int completed_project_id, String title, Date date, double rating){
        Review review = new Review();
        review.setUser_id(user_id);
        review.setCompleted_project_id(completed_project_id);
        review.setTitle(title);
        review.setDate(new java.sql.Date(date.getTime()));
        review.setRating(rating);

        return reviewDao.saveReview(review);
    }

    public List<Review> getReviewsByCompletedProjectId(int id){ return reviewDao.getProjectReviewByProjectId(id); }
}
