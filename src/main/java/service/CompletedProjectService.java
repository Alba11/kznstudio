package service;

import dao.CompletedProjectDao;
import dao.CompletedProjectDaoMySql;
import dto.CompletedProject;
import dto.Project;
import liquibase.pro.packaged.C;

import java.util.Date;
import java.util.List;

public class CompletedProjectService {
    private final CompletedProjectDao completedProjectDao = new CompletedProjectDaoMySql();

    public boolean saveCompletedProject(Project project, Date finish_date){
        CompletedProject completedProject = new CompletedProject();

        completedProject.setProject_id(project.getId());
        completedProject.setCreate_date(new java.sql.Date(finish_date.getTime()));

        return completedProjectDao.saveCompletedProject(completedProject);
    }

    public List<CompletedProject> getCompletedProjectsByUserId(int user_id){ return completedProjectDao.getCompletedProjectsByUser_id(user_id); }

    public List<CompletedProject> getAllCompletedProjects(){ return completedProjectDao.getAllCompletedProjects(); }

    public CompletedProject getCompletedProjectById(int id){ return completedProjectDao.getCompletedProjectById(id); }
}
