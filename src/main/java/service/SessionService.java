package service;

import dao.SessionDao;
import dao.SessionDaoMySql;
import dto.Session;

import java.util.ArrayList;
import java.util.List;

public class SessionService {
    private final SessionDao sessionDao = new SessionDaoMySql();

    public Session getSessionByUserId(int user_id){
        return sessionDao.getSessionByUserId(user_id);
    }

    public Session getSessionById(String session_id){
        return sessionDao.getSessionById(session_id);
    }

    public List<String> enrollSession(String session_id, int user_id) {
        List<String> errors = new ArrayList<>();

        if (session_id == null) errors.add("session is not exist");
        if (user_id < 0) errors.add("session is not have user");
        if (!errors.isEmpty()) return errors;

        Session session = new Session();
        session.setUser_id(user_id);
        session.setSession_id(session_id);

        sessionDao.saveSessionDao(session);
        return null;
    }

    public boolean removeSessionByUserId(int user_id){
        return sessionDao.removeSessionByUserId(user_id);
    }
}
