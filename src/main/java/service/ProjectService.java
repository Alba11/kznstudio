package service;

import dao.CompletedProjectDao;
import dao.CompletedProjectDaoMySql;
import dao.ProjectDAO;
import dao.ProjectDAOMySql;
import dto.CompletedProject;
import dto.Project;

import javax.xml.crypto.Data;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ProjectService {
    private final ProjectDAO projectDAO = new ProjectDAOMySql();
    private final String DATE_FORMAT = "dd/MM/yyyy";
    private final int HOUR = 3600*1000;
    private final SimpleDateFormat FORMAT = new SimpleDateFormat(DATE_FORMAT);
    private final CompletedProjectDao completedProjectDao = new CompletedProjectDaoMySql();

    public boolean saveProject(String name, String image_url, String site_url, int user_id, Date date) throws ParseException {
        Project project = new Project();
        project.setName(name);
        project.setImage_url(image_url);
        project.setSite_url(site_url);
        project.setUser_id(user_id);

        project.setCreate_date(new java.sql.Date(date.getTime()));
        return projectDAO.saveProject(project);
    }

    public List<Project> getAllProjects(){
        return projectDAO.getAllProjects();
    }

    public List<Project> getProjectsByUserId(int user_id){ return projectDAO.getProjectsByUser_id(user_id); }

    public boolean deleteProjectById(int id){ return projectDAO.deleteProjectById(id); }

    public boolean completeProject(int project_id, java.util.Date date) {
        deleteProjectById(project_id);
        CompletedProject completedProject = new CompletedProject();
        completedProject.setProject_id(project_id);
        completedProject.setFinish_date(new java.sql.Date(date.getTime()));
        return completedProjectDao.saveCompletedProject(completedProject);
    }
}
