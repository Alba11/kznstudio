package dao;

import dto.Project;
import dto.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ProjectDAOMySql implements ProjectDAO{
    private final MySqlConnection connection = new MySqlConnection();
    @Override
    public boolean saveProject(Project project) {
        try (Connection conn = connection.getNewConnection()) {
            String sql = "INSERT INTO schema_users.project (`name`, image_url, site_url, user_id, create_date) values (?, ?, ?, ?, ?)";
            System.out.println("Connection is done");
            try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
                preparedStatement.setString(1, project.getName());
                preparedStatement.setString(2, project.getImage_url());
                preparedStatement.setString(3, project.getSite_url());
                preparedStatement.setInt(4, project.getUser_id());
                preparedStatement.setDate(5, project.getCreate_date());

                preparedStatement.executeUpdate();
                System.out.println("yes");
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Project> getAllProjects() {
        try (Connection conn = connection.getNewConnection()){
            String sql = "SELECT * FROM schema_users.project";
            try(PreparedStatement statement = conn.prepareStatement(sql)){
                LinkedList<Project> projects = new LinkedList<>();
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    Project project = new Project();
                    project.setId(resultSet.getInt("id"));
                    project.setName(resultSet.getString("name"));
                    project.setImage_url(resultSet.getString("image_url"));
                    project.setSite_url(resultSet.getString("site_url"));
                    project.setUser_id(resultSet.getInt("user_id"));
                    project.setCreate_date(resultSet.getDate("create_date"));
                    projects.add(project);
                }
                return projects;
            }
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Project> getProjectsByUser_id(int user_id) {
        try (Connection conn = connection.getNewConnection()){
            String sql = "SELECT * FROM schema_users.project WHERE user_id = ? AND is_ready = 0";
            try(PreparedStatement statement = conn.prepareStatement(sql)){
                LinkedList<Project> projects = new LinkedList<>();
                statement.setInt(1, user_id);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    Project project = new Project();
                    project.setId(resultSet.getInt("id"));
                    project.setName(resultSet.getString("name"));
                    project.setImage_url(resultSet.getString("image_url"));
                    project.setSite_url(resultSet.getString("site_url"));
                    project.setUser_id(resultSet.getInt("user_id"));
                    project.setCreate_date(resultSet.getDate("create_date"));
                    projects.add(project);
                }
                return projects;
            }
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteProjectById(int project_id) {
        try (Connection conn = connection.getNewConnection()) {
            String sql = "UPDATE schema_users.project as p SET p.is_ready = 1 WHERE id = ?";
            try (PreparedStatement statement = conn.prepareStatement(sql)) {
                statement.setInt(1, project_id);
                statement.executeUpdate();
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
