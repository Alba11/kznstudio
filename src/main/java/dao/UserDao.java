package dao;

import dto.User;

public interface UserDao {
    User getUserById(int id);

    User getUserByUsername(String username);

    boolean saveUser(User user);

    User findUser(String username);
}
