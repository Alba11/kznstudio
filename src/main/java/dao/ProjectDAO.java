package dao;

import dto.Project;

import java.util.List;

public interface ProjectDAO {
    boolean saveProject(Project project);
    boolean deleteProjectById(int user_id);
    List<Project> getAllProjects();
    List<Project> getProjectsByUser_id(int user_id);
}
