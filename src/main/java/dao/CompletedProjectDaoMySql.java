package dao;

import dto.CompletedProject;
import dto.Project;
import dto.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.util.LinkedList;
import java.util.List;

public class CompletedProjectDaoMySql implements CompletedProjectDao{
    private final MySqlConnection connection = new MySqlConnection();

    @Override
    public boolean saveCompletedProject(CompletedProject completedProject) {
        try (Connection conn = connection.getNewConnection()) {
            String sql = "INSERT INTO schema_users.completed_project (project_id, finish_date) values (?, ?)";
            System.out.println("Connection is done");
            try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
                preparedStatement.setInt(1, completedProject.getProject_id());
                preparedStatement.setDate(2, completedProject.getFinish_date());
                preparedStatement.executeUpdate();
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteCompletedProjectById(int user_id) {
        return false;
    }

    @Override
    public List<CompletedProject> getAllCompletedProjects() {
        try (Connection conn = connection.getNewConnection()){
            String sql = "SELECT * FROM schema_users.completed_project as cp JOIN schema_users.project as p on cp.project_id = p.id";
            try(PreparedStatement statement = conn.prepareStatement(sql)){
                LinkedList<CompletedProject> completedProjects = new LinkedList<>();
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    CompletedProject completedProject = new CompletedProject();
                    completedProject.setId(resultSet.getInt("id"));
                    completedProject.setName(resultSet.getString("name"));
                    completedProject.setImage_url(resultSet.getString("image_url"));
                    completedProject.setSite_url(resultSet.getString("site_url"));
                    completedProject.setUser_id(resultSet.getInt("user_id"));
                    completedProject.setCreate_date(resultSet.getDate("create_date"));
                    completedProject.setFinish_date(resultSet.getDate("finish_date"));
                    completedProjects.add(completedProject);
                }
                return completedProjects;
            }
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<CompletedProject> getCompletedProjectsByUser_id(int user_id) {
        try (Connection conn = connection.getNewConnection()){
            String sql = "SELECT * FROM schema_users.completed_project as cp JOIN schema_users.project as p ON cp.project_id = p.id WHERE p.user_id = ? AND p.is_ready = 1";
            try(PreparedStatement statement = conn.prepareStatement(sql)){
                LinkedList<CompletedProject> completedProjects = new LinkedList<>();
                statement.setInt(1, user_id);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    CompletedProject completedProject = new CompletedProject();
                    completedProject.setId(resultSet.getInt("id"));
                    completedProject.setName(resultSet.getString("name"));
                    completedProject.setImage_url(resultSet.getString("image_url"));
                    completedProject.setSite_url(resultSet.getString("site_url"));
                    completedProject.setUser_id(resultSet.getInt("user_id"));
                    completedProject.setCreate_date(resultSet.getDate("create_date"));
                    completedProject.setFinish_date(resultSet.getDate("finish_date"));
                    completedProjects.add(completedProject);
                    System.out.println(1);
                    System.out.println(completedProject.toString());
                }
                return completedProjects;
            }
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public CompletedProject getCompletedProjectById(int id) {
        try(Connection conn = connection.getNewConnection()){
            String sql = "SELECT * FROM schema_users.completed_project as cp JOIN schema_users.project as p ON cp.project_id = p.id WHERE cp.id = ?";
            try (PreparedStatement statement = conn.prepareStatement(sql)){
                statement.setInt(1, id);
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    CompletedProject completedProject = new CompletedProject();
                    completedProject.setId(resultSet.getInt("id"));
                    completedProject.setName(resultSet.getString("name"));
                    completedProject.setImage_url(resultSet.getString("image_url"));
                    completedProject.setSite_url(resultSet.getString("site_url"));
                    completedProject.setUser_id(resultSet.getInt("user_id"));
                    completedProject.setCreate_date(resultSet.getDate("create_date"));
                    completedProject.setFinish_date(resultSet.getDate("finish_date"));
                    return completedProject;
                }
            }
        }catch (SQLException e){
            e.printStackTrace();

        }
        return null;
    }

    @Override
    public boolean saveCompletedProject(Project project, Date date) {
        return false;
    }
}
