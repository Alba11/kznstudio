package dao;

import dto.Review;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ReviewDaoMySql implements ReviewDao{
    private final MySqlConnection connection = new MySqlConnection();

    @Override
    public boolean saveReview(Review review) {
        try (Connection conn = connection.getNewConnection()) {
            String sql = "INSERT INTO schema_users.review (user_id, completed_project_id, title, `date`, rating) values (?, ?, ?, ?, ?)";
            System.out.println("Connection is done");
            try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
                preparedStatement.setInt(1, review.getUser_id());
                preparedStatement.setInt(2, review.getCompleted_project_id());
                preparedStatement.setString(3, review.getTitle());
                preparedStatement.setDate(4, review.getDate());
                preparedStatement.setDouble(5, review.getRating());

                preparedStatement.executeUpdate();
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Review> getProjectReviewByProjectId(int project_id) {
        try (Connection conn = connection.getNewConnection()){
            String sql = "SELECT * FROM schema_users.review as r JOIN schema_users.completed_project as cp ON r.completed_project_id = cp.id JOIN schema_users.user as u ON r.user_id = u.user_id WHERE r.completed_project_id = ?";
            try(PreparedStatement statement = conn.prepareStatement(sql)){
                statement.setInt(1, project_id);
                LinkedList<Review> reviews = new LinkedList<>();
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    Review review = new Review();
                    review.setId(resultSet.getInt("id"));
                    review.setUser_id(resultSet.getInt("user_id"));
                    review.setCompleted_project_id(resultSet.getInt("completed_project_id"));
                    review.setUsername(resultSet.getString("username"));
                    review.setTitle(resultSet.getString("title"));
                    review.setDate(resultSet.getDate("date"));
                    review.setRating(resultSet.getDouble("rating"));
                    reviews.add(review);
                }
                return reviews;
            }
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }
}
