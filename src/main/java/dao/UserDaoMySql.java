package dao;

import dto.Project;
import dto.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDaoMySql implements UserDao {

    private final MySqlConnection connection = new MySqlConnection();

    @Override
    public User getUserById(int id) {
        try (Connection conn = connection.getNewConnection()) {
            String sql = "SELECT * FROM schema_users.user WHERE user_id = ?";
            try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
                preparedStatement.setInt(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();

                if (resultSet.next()) {
                    User user = new User();
                    user.setUser_id(resultSet.getInt("user_id"));
                    user.setUsername(resultSet.getString("username"));
                    return user;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getUserByUsername(String username) {
        try (Connection conn = connection.getNewConnection()) {
            String sql = "SELECT * FROM schema_users.user WHERE username = ?";
            try (PreparedStatement statement = conn.prepareStatement(sql)) {
                statement.setString(1, username);
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    User user = new User();
                    user.setUser_id(resultSet.getInt("user_id"));
                    user.setUsername(resultSet.getString("username"));
                    return user;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean saveUser(User user) {
        try (Connection conn = connection.getNewConnection()) {
            String sql = "INSERT INTO schema_users.user (username, password) values (?, ?)";
            System.out.println("Connection is done");
            try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
                preparedStatement.setString(1, user.getUsername());
                preparedStatement.setString(2, user.getPassword());

                preparedStatement.executeUpdate();
                System.out.println("yes");
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public User findUser(String username) {
        try (Connection conn = connection.getNewConnection()) {
            String sql = "SELECT * FROM schema_users.user WHERE username = ?";
            try (PreparedStatement statement = conn.prepareStatement(sql)) {
                statement.setString(1, username);
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    User user = new User();
                    user.setUser_id(resultSet.getInt("user_id"));
                    user.setUsername(resultSet.getString("username"));
                    user.setPassword(resultSet.getString("password"));
                    return user;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
