package dao;

import dto.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SessionDaoMySql implements SessionDao {

    private final MySqlConnection connection = new MySqlConnection();

    @Override
    public Session getSessionByUserId(int user_id) {
        try(Connection conn = connection.getNewConnection()){
            String sql = "SELECT * FROM schema_users.user_session WHERE user_id = ?";
            try (PreparedStatement statement = conn.prepareStatement(sql)){
                statement.setInt(1, user_id);
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    Session session = new Session();
                    session.setSession_id(resultSet.getString("session_id"));
                    session.setUser_id(resultSet.getInt("user_id"));
                    return session;
                }
            }
        }catch (SQLException e){
            e.printStackTrace();

        }
        return null;
    }

    @Override
    public Session getSessionById(String session_id) {
        try(Connection conn = connection.getNewConnection()){
            String sql = "SELECT * FROM schema_users.user_session WHERE session_id = ?";
            try (PreparedStatement statement = conn.prepareStatement(sql)){
                statement.setString(1, session_id);
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    Session session = new Session();
                    session.setSession_id(resultSet.getString("session_id"));
                    session.setUser_id(resultSet.getInt("user_id"));
                    return session;
                }
            }
        }catch (SQLException e){
            e.printStackTrace();

        }
        return null;
    }

    @Override
    public boolean saveSessionDao(Session session) {
        try (Connection conn = connection.getNewConnection()){
            String sql = "INSERT INTO schema_users.user_session (session_id, user_id) values(?, ?)";
            try(PreparedStatement statement = conn.prepareStatement(sql)){
                statement.setString(1, session.getSession_id());
                statement.setInt(2, session.getUser_id());

                statement.executeUpdate();
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean removeSessionByUserId(int user_id) {
        try (Connection conn = connection.getNewConnection()){
            String sql = "DELETE FROM schema_users.user_session WHERE user_id = ?";
            try(PreparedStatement statement = conn.prepareStatement(sql)){
                statement.setInt(1, user_id);
                statement.executeUpdate();
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }


}
