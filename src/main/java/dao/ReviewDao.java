package dao;

import dto.Review;

import java.util.List;

public interface ReviewDao {
    boolean saveReview(Review review);
    List<Review> getProjectReviewByProjectId(int project_id);
}
