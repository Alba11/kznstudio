package dao;

import dto.Session;
import dto.User;

public interface SessionDao {
    Session getSessionByUserId(int user_id);
    Session getSessionById(String session_id);
    boolean saveSessionDao(Session session);
    boolean removeSessionByUserId(int user_id);
}
