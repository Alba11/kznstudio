package dao;

import dto.CompletedProject;
import dto.Project;

import java.sql.Date;
import java.util.List;

public interface CompletedProjectDao {
    boolean saveCompletedProject(CompletedProject completedProject);
    boolean deleteCompletedProjectById(int user_id);
    List<CompletedProject> getAllCompletedProjects();
    List<CompletedProject> getCompletedProjectsByUser_id(int user_id);
    CompletedProject getCompletedProjectById(int id);
    boolean saveCompletedProject(Project project, Date date);
}
