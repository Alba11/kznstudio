//package javaBack;
//
//import java.util.HashMap;
//import java.util.UUID;
//
//public class UserDataBase {
//    private static final HashMap<String, User> users;
//    private static final HashMap<String, String> sessions = new HashMap<>();
//
//    static {
//        users = new HashMap<>();
//        users.put("admin", new User("admin", "qwerty"));
//        users.put("user", new User("user", "123"));
//    }
//
//    public static boolean isUserDataCorrect(String username, String password) {
//        User user = users.get(username);
//        if (user == null) return false;
//        String correctPassword = user.getPassword();
//        return correctPassword.equals(password);
//    }
//
//    public static boolean correctUserData(String username, String password) {
//        User user = users.get(username);
//        if (user == null) return false;
//        String correctPassword = user.getPassword();
//        return correctPassword.equals(password);
//    }
//
//    public static void addNewUser(String username, String password) {
//        User newUser = new User(username, password);
//        users.put(username, newUser);
//    }
//
//    public static String generateSessionID(String username) {
//        sessions.values().remove(username);
//        String id;
//        do {
//            id = UUID.randomUUID().toString();
//        } while (sessions.containsKey(id));
//        sessions.put(id, username);
//        return id;
//    }
//
//    public static String getSessionForID(String sessionID) {
//        return sessions.get(sessionID);
//    }
//
//    public static User getUser(String username) {
//        return users.get(username);
//    }
//
//    public static void deleteSessionForUser(String username) {
//        sessions.values().remove(username);
//    }
//}
