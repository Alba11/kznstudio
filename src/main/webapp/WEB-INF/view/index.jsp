<%--
  Created by IntelliJ IDEA.
  User: albertahmadiev
  Date: 16.10.2020
  Time: 13:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Kazan Development Studio</title>
    <link rel="icon" type="image/x-icon" href="../../resources/images/icons/favicon.ico"/>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
          type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="../../resources/css/main.css" rel="stylesheet"/>
</head>
<body id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index">Kazan Development Studio</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars ml-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="service">Services</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="works">Our works</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="about">About</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="profile">Profile</a></li>
                <% if (request.getSession().getAttribute("user") == null) { %>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signIn">Sign In</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signUp">Sign Up</a></li>
                <% } else { %>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signOut">Log Out</a></li>
                <% } %>
            </ul>
        </div>
    </div>
</nav>
<!-- Masthead-->
<header class="masthead">
    <div class="container">
        <div class="masthead-subheading">Welcome To Kazan Development Studio!</div>
        <div class="masthead-heading text-uppercase">It's Nice To Meet You</div>
        <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="service">Tell Me More</a>
    </div>
</header>
<!-- Services-->
<section class="page-section" id="services">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Services</h2>
            <h3 class="section-subheading text-muted">We would glad make your business is more productive</h3>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fas fa-shopping-cart fa-stack-1x fa-inverse"></i>
                        </span>
                <h4 class="my-3">E-Commerce</h4>
                <p class="text-muted">We can make to sell your product.</p>
            </div>
            <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fas fa-laptop fa-stack-1x fa-inverse"></i>
                        </span>
                <h4 class="my-3">Responsive Design</h4>
                <p class="text-muted">We make it the most beautiful.</p>
            </div>
            <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fas fa-lock fa-stack-1x fa-inverse"></i>
                        </span>
                <h4 class="my-3">Web Security</h4>
                <p class="text-muted">And of course nobody can't crack your app.</p>
            </div>
        </div>
    </div>
</section>
<!-- About-->
<section class="page-section" id="about">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">How to start your project with us</h2>
            <h3 class="section-subheading text-muted">.</h3>
        </div>
        <ul class="timeline">
            <li>
                <div class="timeline-image"><img class="rounded-circle img-fluid"
                                                 src="../../resources/images/1024px-NYCS-bull-trans-1.svg.png" alt=""/>
                </div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>First<br/>step</h4>
                        <h4 class="subheading">Register on our site</h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">It's so easy. You just need to come up with a
                        username and password.</p></div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-image"><img class="rounded-circle img-fluid"
                                                 src="../../resources/images/1024px-NYCS-bull-trans-2.svg.png" alt=""/>
                </div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>Second<br/>step</h4>
                        <h4 class="subheading"></h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Choose the application that you like. But if you
                        have doubts about the choice, we will be happy to help you with the choice.</p></div>
                </div>
            </li>
            <li>
                <div class="timeline-image"><img class="rounded-circle img-fluid"
                                                 src="../../resources/images/NYCS-bull-trans-3.svg.png" alt=""/></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>Third<br/>step</h4>
                        <h4 class="subheading"></h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Track the progress of your project directly on the
                        site!</p></div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-image"><img class="rounded-circle img-fluid"
                                                 src="../../resources/images/1200px-NYCS-bull-trans-4-red.svg.png"
                                                 alt=""/></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>Fourth<br/>step</h4>
                        <h4 class="subheading"></h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Manage your project right on the site! If you want
                        to add something to your project or fix it, we will always revise it and do everything on time
                        anyway.</p></div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-image">
                    <h4>
                        Be Part
                        <br/>
                        Of Our
                        <br/>
                        Story!
                    </h4>
                </div>
            </li>
        </ul>
    </div>
</section>

<!-- Bootstrap core JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<!-- Core theme JS-->
<script src="../../resources/js/main.js"></script>
<script src="../../resources/js/scripts.js"></script>
</body>
</html>

