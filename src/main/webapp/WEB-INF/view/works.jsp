<%--
  Created by IntelliJ IDEA.
  User: albertahmadiev
  Date: 26.10.2020
  Time: 09:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Our works</title>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
          type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="../../resources/css/main.css" rel="stylesheet"/>
</head>
<body id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index">Kazan Development Studio</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars ml-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="service">Services</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="works">Our works</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="about">About</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="profile">Profile</a></li>
                <% if (request.getSession().getAttribute("user") == null) { %>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signIn">Sign In</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signUp">Sign Up</a></li>
                <% } else { %>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signOut">Log Out</a></li>
                <% } %>
            </ul>
        </div>
    </div>
</nav>

<header class="masthead-works">
    <div class="container">
        <div class="masthead-subheading">You can find a good projects there</div>
        <div class="masthead-heading text-uppercase">We glad to make your project!</div>
        <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="about">Tell Me More</a>
    </div>
</header>

<!-- Portfolio Grid-->
<section class="page-section bg-light" id="portfolio">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Our Completed projects!</h2>
            <h3 class="section-subheading text-muted">We had a lot of clients...</h3>
        </div>
        <div class="row">
            <c:forEach var="work" items="${works}">
                <div class="col-lg-4 col-sm-6 mb-4">
                    <div class="portfolio-item">
                        <a class="portfolio-link" data-toggle="modal" href="${work.getSite_url()}">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="img?filename=${work.getImage_url()}" alt=""/>
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">${work.getName()}</div>
                            <div class="portfolio-caption-subheading text-muted">${work.getCreate_date()}</div>
                            <form action="review?completed_project_id=${work.getId()}" method="post">
                                <button type="submit" class="btn btn-success">Send award</button>
                            </form>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<!-- Core theme JS-->
<script src="../../resources/js/main.js"></script>
<script src="../../resources/js/scripts.js"></script>
</body>
</html>
