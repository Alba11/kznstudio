<%--
  Created by IntelliJ IDEA.
  User: rishatlatypov
  Date: 30.10.2020
  Time: 21:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Kazan Development Studio</title>
    <link rel="icon" type="image/x-icon" href="../../resources/images/icons/favicon.ico"/>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
          type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="../../resources/css/main.css" rel="stylesheet"/>
</head>
<body id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index">Kazan Development Studio</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars ml-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="service">Services</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="works">Our works</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="about">About</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="profile">Profile</a></li>
                <% if (request.getSession().getAttribute("user") == null) { %>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signIn">Sign In</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signUp">Sign Up</a></li>
                <% } else { %>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signOut">Log Out</a></li>
                <% } %>
            </ul>
        </div>
    </div>
</nav>

<!-- Team-->
<section class="page-section bg-light masthead-about" id="team">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Our Amazing Team</h2>
            <h3 class="section-subheading text-muted">These people do your projects in good faith!</h3>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="../../resources/images/indexImage/team/rishat.png" alt=""/>
                    <h4>Latypov Rishat</h4>
                    <p class="text-muted">Back-end Dev</p>
                    <a class="btn btn-dark btn-social mx-2" href="https://www.instagram.com/rishatlatypov/?hl=ru"><i
                            class="fab fa-instagram"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-mail-forward"></i></a>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="../../resources/images/indexImage/team/alba.png" alt=""/>
                    <h4>Akhmadiev ALbert</h4>
                    <p class="text-muted">Front-end Dev</p>
                    <a class="btn btn-dark btn-social mx-2" href="https://www.instagram.com/virgin_alba/?hl=ru"><i
                            class="fab fa-instagram"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="https://www.facebook.com/alik.tok.56"><i
                            class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-mail-forward"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 mx-auto text-center"><p class="large text-muted">We can do everything...</p></div>
        </div>
    </div>
</section>
<!-- Footer-->
<footer class="footer py-4">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4 text-lg-left">Alba&Rishat © Company 2020</div>
            <div class="col-lg-4 my-3 my-lg-0">
                <a class="btn btn-dark btn-social mx-6" href="#!"><i class="fab fa-instagram"></i></a>
            </div>
            <div class="col-lg-4 text-lg-right">
                <a class="mr-3" href="#!">Call us</a>
                <a href="#!">We like it</a>
            </div>
        </div>
    </div>
</footer>

<!-- Bootstrap core JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<!-- Contact form JS-->
<script src="assets/mail/jqBootstrapValidation.js"></script>
<script src="assets/mail/contact_me.js"></script>
<!-- Core theme JS-->
<script src="../../resources/js/main.js"></script>
<script src="../../resources/js/scripts.js"></script>
</body>
</html>
