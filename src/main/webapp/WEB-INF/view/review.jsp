<%@ page import="dto.CompletedProject" %>
<%--
  Created by IntelliJ IDEA.
  User: albertahmadiev
  Date: 05.11.2020
  Time: 20:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Review</title>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
          type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="../../resources/css/main.css" rel="stylesheet"/>
    <link href="../../resources/css/_form-control.scss" rel="stylesheet"/>
</head>
<body id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index">Kazan Development Studio</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars ml-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="service">Services</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="works">Our works</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="about">About</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="profile">Profile</a></li>
                <% if (request.getSession().getAttribute("user") == null) { %>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signIn">Sign In</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signUp">Sign Up</a></li>
                <% } else { %>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signOut">Log Out</a></li>
                <% } %>
            </ul>
        </div>
    </div>
</nav>
<header class="masthead">
    <div class="container">
        <section class="page-section">
            <h2 class="section-heading text-uppercase">${project.getName()}</h2><br>
        </section>
        <img src="img?filename=${project.getImage_url()}" width="500px"><br>
        <h3>Create date:${project.getCreate_date()}</h3><br>
        <h3>Finish date:${project.getFinish_date()}</h3><br>
        <center>
            <table class="table table-dark">
                <tr>
                    <td>Id</td>
                    <td>Username</td>
                    <td>Title</td>
                    <td>Rating</td>
                    <td>Date</td>
                    <td></td>
                </tr>
                <c:forEach var="review" items="${reviews}">
                    <tr>
                        <td>${review.getId()}</td>
                        <td>${review.getUsername()}</td>
                        <td>${review.getTitle()}</td>
                        <td>${review.getRating()}</td>
                        <td>${review.getDate()}</td>
                    </tr>
                </c:forEach>
            </table>
        </center>
        <br>

        <form method="post" action="createReview?project_id=${project.getId()}&">
            <h3><label for="title">Your review:</label><br></h3>
            <input class="form-control" id="title" type="text" name="title" placeholder="review text"><br>
            <h3><label for="rating">Put your rating</label></h3>
            <p><select class="custom-select custom-select-lg mb-3" id="rating" name="rating" size="5" multiple>
                <option selected value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select><br>
                <button class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" type="submit">Send</button>
            </p>
        </form>
    </div>
</header>

<!-- Bootstrap core JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<!-- Core theme JS-->
<script src="../../resources/js/main.js"></script>
<script src="../../resources/js/scripts.js"></script>
</body>
</html>
