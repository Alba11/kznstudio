<%--
  Created by IntelliJ IDEA.
  User: albertahmadiev
  Date: 16.10.2020
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Services</title>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
          type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="../../resources/css/main.css" rel="stylesheet"/>
</head>
<body id="page-top">

<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index">Kazan Development Studio</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigatio  n">
            Menu
            <i class="fas fa-bars ml-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="service">Services</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="works">Our works</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="about">About</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="profile">Profile</a></li>
                <% if (request.getSession().getAttribute("user") == null) { %>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signIn">Sign In</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signUp">Sign Up</a></li>
                <% } else { %>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signOut">Log Out</a></li>
                <% } %>
            </ul>
        </div>
    </div>
</nav>


<!-- Masthead-->
<header class="masthead-services">
    <div class="container">
        <div class="masthead-subheading">Welcome To KZN Studio!</div>
        <div class="masthead-heading text-uppercase">It's Nice To Meet You</div>
        <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="service">Tell Me More</a>
    </div>
</header>

<div class="container">
    <div class="card-deck mb-3 text-center">
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <img src="../../resources/images/ios.png" width="318" height="570">
                <h4 class="my-0 font-weight-normal">iOS app</h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">$10000 <small class="text-muted">/ mo</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>10 users included</li>
                    <li>2 GB of storage</li>
                    <li>Email support</li>
                    <li>Help center access</li>
                </ul>

                <% if (request.getSession().getAttribute("user") == null) { %>
                <button type="button" class="btn btn-lg btn-block btn-outline-primary"><a href="signIn">Sign in for
                    free</a></button>
                <% } else { %>
                <button type="button" class="btn btn-lg btn-block btn-primary"><a href="iosApp" style="color: white">Get
                    it</a></button>
                <% } %>

            </div>
        </div>
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <img src="../../resources/images/android.png" width="318" height="570">
                <h4 class="my-0 font-weight-normal">Android app</h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">$1000 <small class="text-muted">/ mo</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>20 users included</li>
                    <li>10 GB of storage</li>
                    <li>Priority email support</li>
                    <li>Help center access</li>
                </ul>
                <% if (request.getSession().getAttribute("user") == null) { %>
                <button type="button" class="btn btn-lg btn-block btn-outline-primary"><a href="signIn">Sign in for
                    free</a></button>
                <% } else { %>
                <button type="button" class="btn btn-lg btn-block btn-primary"><a href="profile" style="color: white">Get
                    it</a></button>
                <% } %>
            </div>
        </div>

        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <img src="../../resources/images/61xzMLlyCBL.jpg" width="318" height="570">
                <h4 class="my-0 font-weight-normal">Web app</h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">$500 <small class="text-muted">/ mo</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>30 users included</li>
                    <li>15 GB of storage</li>
                    <li>Phone and email support</li>
                    <li>Help center access</li>
                </ul>
                <% if (request.getSession().getAttribute("user") == null) { %>
                <button type="button" class="btn btn-lg btn-block btn-outline-primary"><a href="signIn">Sign in for
                    free</a></button>
                <% } else { %>
                <button type="button" class="btn btn-lg btn-block btn-primary"><a href="profile" style="color: white">Get
                    it</a></button>
                <% } %>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../../assets/js/vendor/popper.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>
<script src="../../assets/js/vendor/holder.min.js"></script>
<script>
    Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
    });
</script>
<script src="../../resources/js/validation.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="../../resources/js/scripts.js"></script>
<script src="../../resources/js/main.js"></script>
</body>
</html>
