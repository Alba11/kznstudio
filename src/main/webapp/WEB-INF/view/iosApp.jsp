<%--
  Created by IntelliJ IDEA.
  User: albertahmadiev
  Date: 03.11.2020
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>iOSApp</title>
    <link rel="icon" type="image/x-icon" href="../../resources/images/icons/favicon.ico"/>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
          type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="../../resources/css/main.css" rel="stylesheet"/>
    <link href="../../resources/css/_form-control.scss" rel="stylesheet"/>
</head>
<body id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index">Kazan Development Studio</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars ml-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="service">Services</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="works">Our works</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="about">About</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="profile">Profile</a></li>
                <% if (request.getSession().getAttribute("user") == null) { %>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signIn">Sign In</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signUp">Sign Up</a></li>
                <% } else { %>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="signOut">Log Out</a></li>
                <% } %>
            </ul>
        </div>
    </div>
</nav>

<header class="masthead_1">
    <div class="container">
        <form method="post" action="createProject" enctype="multipart/form-data">

            <section class="page-section">

                <h2 class="section-heading text-uppercase"><label for="project-image">Project image:</label></h2>
                <div class="custom-file">
                    <input class="custom-file-input" id="project-image" type="file" name="file">
                    <label class="custom-file-label" for="project-image">Choose file...</label></div>
                <br>

                <h2 class="section-heading text-uppercase"><label for="project-name">Project name:</label></h2>
                <input class="form-control form-control-lg" id="project-name" type="text" name="project_name"
                       placeholder="Your project name"><br>

                <h2 class="section-heading text-uppercase"><label for="project-url">Project url:</label></h2>
                <input class="form-control form-control-lg" id="project-url" type="url" name="project_url"
                       placeholder="Project url"><br>

                <input class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" type="submit"
                       name="submit_image"
                       value="Upload">
            </section>
        </form>
    </div>
</header>


<!-- Bootstrap core JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<!-- Core theme JS-->
<script src="../../resources/js/main.js"></script>
<script src="../../resources/js/scripts.js"></script>
</body>
</html>
